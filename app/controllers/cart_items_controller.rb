class CartItemsController < ApplicationController

    def create
        @cart_item = CartItem.new(qty: params[:qty], product_id: params[:product_id], cart_id: session[:cart_id])
        if @cart_item.save
            redirect_to root_path
        else
            render "products/index"
        end
    end
    def destroy
        cart_item = CartItem.find(params[:id])
        cart_item.destroy
        redirect_to root_path
    end
end
