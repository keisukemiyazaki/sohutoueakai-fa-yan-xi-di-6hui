class CartsController < ApplicationController
    def show
        @cart = Cart.find(params[:id])
        @cart_items = CartItem.where(cart_id: session[:cart_id])
        @sum = 0
        @cart_items.each do |c|
            @sum += c.product.price*c.product.qty
        end
    end
end
