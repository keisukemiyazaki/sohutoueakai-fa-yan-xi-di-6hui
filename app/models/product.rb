class Product < ActiveRecord::Base
    has_many :cart_items, dependent: :destroy
    validates :name,:price, presence: true
    validates :price, numericality: {only_integer: true, greater_than: 0}
end
