class CartItem < ActiveRecord::Base
    belongs_to :product
    belongs_to :cart
    validates :qty,:product_id,:cart_id,presence: true
    validates :qty, numericality: {only_integer: true, greater_than: 0}
end
